from owlready2 import *

onto = get_ontology("http://www.semantic.bosch.com/ae-so/genericdatamodel")

with onto:
    
    ####### classes definition########
    
    class Infrastructure(Thing):
        pass
    
    class Trace(Thing):
        pass
    
    class Strategy(Thing):
        pass
    
    class Snapshot(Thing):
        pass
    
    class Process(Thing):
        pass
    
    class Product(Thing):
        pass
    
    class Future(Thing):
        pass
    
    class Activity(Process):
        label = "Activity"
        comment = "class that"
        pass
    class ActivityEdge(Process):
        pass
    class AuxResource(Infrastructure):
        pass
    class AuxResourceRequirement(Process):
        pass
    class BillOfMaterials(Product):
        pass
    class Calendar(Infrastructure):
        pass
    class Capability(Process):
        comment = "Ratio of the theoretical available recipes at a tool and the available recipes "
        pass
    class CapabilitySnapshot(Snapshot):
        pass
    
    class Cluster(Infrastructure):
        pass
    
    class CurrentMachineState(Snapshot):
        pass
    class CurrentWip(Snapshot):
        label = "Current Wip"
        pass
    
    class CycleTimeCommitment(Strategy):
        pass
    class ChamberDedication(Snapshot, Trace):
        pass
    
    class DeliveryCommittment(Strategy):
        pass
    class DemandSnapshot(Snapshot):
        pass
    class DemandTrace(Trace):
        pass
    
    class AddEquipment(Future):
        pass
    
    class RemoveEquipment(Future):
        pass
    
    class AddDisturbance(Future):
        pass
    
    class AddNewDedication(Future):
        pass
    
    class AddNewCapability(Future):
        pass
    
    
    class Equipment(Cluster):
        label = "Equipment"
        comment = "Equipment name. Machine of one equipment type that can execute certain production (or measurement) steps  "
        pass
    class Chamber(Equipment):
        pass
    
    class EquipmentCapability(Trace):
        pass
    class EquipmentGroup(Infrastructure):
        label = "Equipment Group"
        comment = "Equipment Group Name. Group of same type of machines for production of certain production steps "
        pass
    class EquipmentState(Trace):
        pass
    class Fab(Infrastructure):
        label="Fab"
        comment="The name of the fab producing the product. Production Area"
        pass
    class Location(Infrastructure):
        pass
    class LotEvent(Trace):
        comment = "Gives the history of lot events (Processing, Hold, Split, Merge, etc.) and equivalently to the lots, the equipment also has to be historically tracked "
        pass
    class LotState(Trace):
        comment = "Covers all historical processes start and end of a lot in any facility and provides the full history of operations of a specific lot "
        pass
    class Maintenance(Infrastructure):
        comment = "Includes all different maintenance types and their durations and it can be described several times and can vary slightly between the equipment "
        pass
    class Material(Infrastructure):
        pass
    class MaterialConsumption(Process):
        pass
    class MaterialTransfer(Infrastructure):
        pass
    class MaxLotSize(Infrastructure):
        pass
    class Operation(Process):
        pass
    class Plant(Infrastructure):
        comment = "Production location with decentral planning and control unit "
        pass
    class ProductLevel2(Product):
        label = "Product Level 2"
        comment = "Product Name"
        pass
    class ProductLevel3(Product):
        pass
    class ProductLevel4(Product):
        pass
    class Recipe(Process):
        label = "Recipe"
        comment = "Recipe required at a specific step. The production pattern of a specific production step on equipment "
        pass
    class RecipeEquipmentConfig(Process):
        pass
    class RecipeGroup(Process):
        pass
    class ReleasePlan(Strategy):
        comment = "Quantity planned to be released to a plant or facility "
        pass
    class Route(Process):
        label = "Route"
        comment = "Route name. Define the route that produces a product. Order of production steps for one product"
        pass
    class RouteSequence(Process):
        pass
    class Setup(Infrastructure):
        comment = "Tool status for preparing for the next lot "
        pass
    class ShiftAttendance(Trace):
        pass
    class SupplyChain(Infrastructure):
        pass
    class TargetCosts(Strategy):
        pass
    class TargetRevenue(Strategy):
        pass
    class TargetServiceLevel(Strategy):
        pass
    class TargetWip(Strategy):
        pass
    class Team(Infrastructure):
        pass
    class Technology(Product):
        pass
    class TimeConstraint(Process):
        pass
    class Kanban(Process):
        pass
    class WorkerQualification(Infrastructure):
        pass
    class Workplan(Process):
        pass
    class Workshop(Infrastructure):
        label ="Workshop"
        pass
    
    ############Object Properties - relationships between classes#######################
    
    class belongsToActivity(ObjectProperty):
        domain           = [Activity]
        range            = [Workplan]
        
    class hasActivity(ObjectProperty):
        domain           = [Workplan]
        range            = [Activity]
        inverse_property = belongsToActivity
        
    class hasActivityEdge(ObjectProperty): 
    	range           = [ActivityEdge]
    	domain          = [Activity]
        
    class hasActivityEdge(ObjectProperty): 
    	range           = [ActivityEdge]
    	domain          = [Workplan]
        
    class hasAuxResourceRequirement(ObjectProperty):
        
        range = [AuxResourceRequirement]
        domain = [Operation]
        #owl:minCardinality = 1  # Indicates at least one requirement is needed.
        #is_a = [hasAuxResourceRequirement.min(cardinality, 1)]
    
    Operation.is_a.append(hasAuxResourceRequirement.min(1,AuxResourceRequirement))
    
    
    class hasAuxResourceRequirement(ObjectProperty): 
    	range           = [AuxResourceRequirement]
    	domain          = [AuxResource]
    
    class isAssembledByBillOfMaterials(ObjectProperty): 
    	range           = [BillOfMaterials]
    	domain          = [ProductLevel4]
    
#    class hasCalendar(ObjectProperty): 
#    	range           = [Calendar]
#    	domain          = [Workshop] 

    
#    class hasCalendar(ObjectProperty): 
#    	range           = [Calendar]
#    	domain          = [Team] 
    
    class hasCalendar(ObjectProperty): 
    	range           = [Calendar]
    	domain          = [Fab, Team, Workshop] 
    Fab.is_a.append(hasCalendar.max(1,Calendar))
    
    
    class hasCapabilitySnapshot(ObjectProperty): 
    	range           = [CapabilitySnapshot]
    	domain          = [Capability] 
    
    class hasCapabilitySnapshot(ObjectProperty):
        range           = [CapabilitySnapshot]
        domain          = [Equipment]
        #owl:minCardinality = 1  # Indicates at least one requirement is needed.
    
    #Equipment.is_a.append(hasCapabilitySnapshot.exactly(1,CapabilitySnapshot))
    
    class hasCurrentMachineState(ObjectProperty): 
    	range           = [CurrentMachineState]
    	domain          = [Equipment] 
    Equipment.is_a.append(hasCurrentMachineState.exactly(1,CurrentMachineState))
    
    
    class hasCurrentWip(ObjectProperty): 
    	range           = [CurrentWip]
    	domain            = [DemandSnapshot, Operation, Equipment, ProductLevel4] 
        

        
#    class hasCurrentWip(ObjectProperty): 
#    	range           = [Current_Wip]
#    	domain            = [Operation] 
    
#    class hasCurrentWip(ObjectProperty): 
#    	range           = [Current_Wip]
#    	domain            = [Equipment] 
#    Equipment.is_a.append(hasCurrentWip.exactly(1,CurrentWip))
    
    
#    class hasCurrentWip(ObjectProperty): 
#    	range           = [Current_Wip]
#    	domain            = [Product_Level_4] 
    
    class tracksCurrentWip(ObjectProperty): 
    	range           = [CurrentWip]
    	domain            = [LotState] 
    
    class hasCurrentWip(ObjectProperty): 
    	range           = [CurrentWip]
    	domain            = [EquipmentState] 
   # EquipmentState.is_a.append(hasCurrentWip.exactly(1,CurrentWip))
   
    class addNewEquipment(ObjectProperty): 
    	range           = [Equipment]
    	domain            = [AddEquipment] 
    
    class removeEquipment(ObjectProperty): 
    	range           = [Equipment]
    	domain            = [RemoveEquipment] 
        
    class addDisturbanceEquipment(ObjectProperty): 
    	range           = [Equipment]
    	domain            = [AddDisturbance] 
        
    class addNewDedicationEquipmentGroup(ObjectProperty): 
    	range           = [EquipmentGroup]
    	domain            = [AddNewDedication] 
   
    class addNewCapabilityEquipmentGroup(ObjectProperty): 
    	range           = [EquipmentGroup]
    	domain            = [AddNewCapability] 
        
    class hasCycleTimeCommitment(ObjectProperty): 
    	range           = [CycleTimeCommitment]
    	domain            = [Fab] 
    Fab.is_a.append(hasCycleTimeCommitment.exactly(1,CycleTimeCommitment))
    
    class hasCycleTimeCommitment(ObjectProperty): 
    	range           = [CycleTimeCommitment]
    	domain            = [SupplyChain] 
    SupplyChain.is_a.append(hasCycleTimeCommitment.exactly(1,CycleTimeCommitment))
    
    
    class hasCycleTimeCommitment(ObjectProperty): 
    	range           = [CycleTimeCommitment]
    	domain            = [ProductLevel4] 
    
    class hasDeliveryCommittment(ObjectProperty): 
    	range           = [DeliveryCommittment]
    	domain            = [Fab] 
    Fab.is_a.append(hasDeliveryCommittment.exactly(1,DeliveryCommittment))
    
    class hasDeliveryCommittment(ObjectProperty): 
    	range           = [DeliveryCommittment]
    	domain            = [SupplyChain] 
    SupplyChain.is_a.append(hasDeliveryCommittment.exactly(1,DeliveryCommittment))
    
    class hasDeliveryCommittment(ObjectProperty): 
    	range           = [DeliveryCommittment]
    	domain            = [ProductLevel4] 
    
    class hasDemandSnapshot(ObjectProperty): 
    	range           = [DemandSnapshot]
    	domain            = [ProductLevel2] 
    
    class hasDemandSnapshot(ObjectProperty): 
    	range           = [DemandSnapshot]
    	domain            = [ProductLevel3] 
    
    class hasDemandSnapshot(ObjectProperty): 
    	range           = [DemandSnapshot]
    	domain            = [ProductLevel4] 
    
    class hasDemandTrace(ObjectProperty): 
    	range           = [DemandTrace]
    	domain            = [ProductLevel2] 
    
    class hasDemandTrace(ObjectProperty): 
    	range           = [DemandTrace]
    	domain            = [ProductLevel3] 
    
    class hasDemandTrace(ObjectProperty): 
    	range           = [DemandTrace]
    	domain            = [ProductLevel4] 
    
    class isPerformedAtEquipment(ObjectProperty): 
    	range           = [Equipment]
    	domain            = [Maintenance] 
#    Maintenance.is_a.append(isPerformedAt.exactly(1,Equipment))
    
    
    class requiresEquipmentCapability(ObjectProperty): 
    	range           = [EquipmentCapability]
    	domain            = [Operation] 
    
    class hasEquipmentCapability(ObjectProperty): 
    	range           = [EquipmentCapability]
    	domain            = [Equipment] 
    
    class consistsOfEquipmentGroup(ObjectProperty): 
    	range           = [EquipmentGroup]
    	domain            = [Fab] 
#    Fab.is_a.append(consistsOfEquipmentGroup.min(1,EquipmentGroup))

    class containsEquipment(ObjectProperty): 
    	range           = [Equipment]
    	domain            = [EquipmentGroup] 
    
    class isRequiredByRecipe(ObjectProperty): 
    	range           = [Recipe]
    	domain            = [ChamberDedication] 
        
    class canBeProcessedInChamber(ObjectProperty): 
    	range           = [ChamberDedication]
    	domain            = [CurrentWip] 
    
    class hasChildChamber(ObjectProperty):
        range           = [Chamber]
        domain            = [Equipment]
    
    class hasParentEquipment(ObjectProperty):
        range           = [Equipment]
        domain            = [Chamber,ChamberDedication]
#        inverse_property = hasChildChamber
    
    class hasDedication(ObjectProperty):
        range           = [ChamberDedication]
        domain            = [Chamber]
        
        
    class isRequiredByEquipment(ObjectProperty): 
    	range           = [Equipment]
    	domain            = [Operation] 
        
    class isPerformedAtFab(ObjectProperty): 
    	range           = [Fab]
    	domain            = [ChamberDedication] 
    
    class isProducedByEquipmentGroup(ObjectProperty): 
    	range           = [EquipmentGroup]
    	domain            = [ProductLevel3] 
    
    class belongsToEquipmentGroup(ObjectProperty):
#        inverse_property = containsEquipment
        range = [EquipmentGroup]
        domain = [Equipment]

    
    class hasAStateEquipmentState(ObjectProperty): 
    	range           = [EquipmentState]
    	domain            = [Equipment] 
    Equipment.is_a.append(hasAStateEquipmentState.min(1,EquipmentState))
    
    class hasEquipment(ObjectProperty):
        range           = [Equipment]
        domain            = [EquipmentState]
#        inverse_property = hasAStateEquipmentState
    
    class requiresEquipmentState(ObjectProperty): 
    	range           = [EquipmentState]
    	domain            = [Maintenance] 
    EquipmentState.is_a.append(requiresEquipmentState.max(1,Maintenance))
    
    class consistsOfFab(ObjectProperty): 
    	range           = [Fab]
    	domain            = [Plant] 

    
    class isLocatedInLocation(ObjectProperty): 
    	range           = [Location]
    	domain            = [Fab] 
    Fab.is_a.append(isLocatedInLocation.exactly(1,Location))
    
    class connectedToLotEvent(ObjectProperty): 
    	range           = [LotEvent]
    	domain            = [Operation] 
    
    class connectedToLotState(ObjectProperty): 
    	range           = [LotState]
    	domain            = [Operation] 
    
    class requiresLotState(ObjectProperty): 
    	range           = [LotState]
    	domain            = [RouteSequence] 
    
    class hasLotState(ObjectProperty): 
    	range           = [LotState]
    	domain            = [ProductLevel4] 
    
    class triggersMaterialConsumption(ObjectProperty): 
    	range           = [MaterialConsumption]
    	domain            = [Operation] 
    
    class hasMaterialConsumption(ObjectProperty): 
    	range           = [MaterialConsumption]
    	domain            = [Material] 
    
    class hasMaterialTransfer(ObjectProperty): 
    	range           = [MaterialTransfer]
    	domain            = [Equipment] 
        
    class hasMaxLotSize(ObjectProperty): 
    	range           = [MaxLotSize]
    	domain            = [Fab] 
    Fab.is_a.append(hasMaxLotSize.exactly(1,MaxLotSize))
    
    
    class hasMaxLotSize(ObjectProperty): 
    	range           = [MaxLotSize]
    	domain            = [ProductLevel3] 
    
    class containsOperation(ObjectProperty): 
    	range           = [Operation]
    	domain            = [Capability] 
    
    class containsOperation(ObjectProperty): 
    	range           = [Operation]
    	domain            = [Recipe] 
    
    class consistsOfPlant(ObjectProperty): 
    	range           = [Plant]
    	domain            = [SupplyChain] 
        
    class belongsToSupplyChain(ObjectProperty): 
    	range           = [SupplyChain]
    	domain            = [Plant] 
    
    class hasProductLevel2(ObjectProperty): 
    	range           = [ProductLevel2]
    	domain            = [Technology] 
    
    class hasProductLevel2(ObjectProperty): 
    	range           = [ProductLevel3]
    	domain            = [ProductLevel2] 
    
    class isReleasingProductLevel4(ObjectProperty): 
    	range           = [ProductLevel4]
    	domain            = [ReleasePlan] 
    	
    class hasProductLevel4(ObjectProperty): 
    	range           = [ProductLevel4]
    	domain            = [Workplan] 
    
    class hasProductLevel4(ObjectProperty): 
    	range           = [ProductLevel4]
    	domain            = [ProductLevel3] 
    
    class containsRecipe(ObjectProperty): 
    	range           = [Recipe]
    	domain            = [RecipeGroup] 
    
    class isDefinedByRecipeEquipmentConfig(ObjectProperty): 
    	range           = [RecipeEquipmentConfig]
    	domain            = [Recipe] 
    
    class requiresRecipeEquipmentConfig(ObjectProperty): 
    	range           = [RecipeEquipmentConfig]
    	domain            = [Equipment] 
    Equipment.is_a.append(requiresRecipeEquipmentConfig.min(1,RecipeEquipmentConfig))
    
    
    class receivesReleasePlan(ObjectProperty): 
    	range           = [ReleasePlan]
    	domain            = [Fab] 
    Fab.is_a.append(receivesReleasePlan.min(1,ReleasePlan))
    
    class hasReleasePlan(ObjectProperty): 
    	range           = [ReleasePlan]
    	domain            = [SupplyChain] 
    SupplyChain.is_a.append(hasReleasePlan.exactly(1,ReleasePlan))
    
    class isUsingRoute(ObjectProperty): 
    	range           = [Route]
    	domain            = [ProductLevel4] 
    
    class isDefinedByRoute(ObjectProperty): 
    	range           = [Route]
    	domain            = [RouteSequence] 
    
    class isSpecifiedByRouteSequence(ObjectProperty): 
    	range           = [RouteSequence]
    	domain            = [Activity] 
    
    class isRequiredByRouteSequence(ObjectProperty): 
    	range           = [RouteSequence]
    	domain            = [Operation] 
    
    class hasSetup(ObjectProperty): 
    	range           = [Setup]
    	domain            = [RecipeGroup] 
    
    class definedBySetup(ObjectProperty): 
    	range           = [Setup]
    	domain            = [Equipment] 
    
    class belongsToSupplyChain(ObjectProperty): 
    	range           = [SupplyChain]
    	domain            = [Fab] 
    Fab.is_a.append(belongsToSupplyChain.exactly(1,SupplyChain))
    
    class hasTargetCosts(ObjectProperty): 
    	range           = [TargetCosts]
    	domain            = [Technology] 
    
    class hasTargetRevenue(ObjectProperty): 
    	range           = [TargetRevenue]
    	domain            = [Technology] 
    
    class hasTargetServiceLevel(ObjectProperty): 
    	range           = [TargetServiceLevel]
    	domain            = [Technology] 
    
    class hasTargetWip(ObjectProperty): 
    	range           = [TargetWip]
    	domain            = [ProductLevel4] 
    
    class consistsOfTeam(ObjectProperty): 
    	range           = [Team]
    	domain            = [Workshop] 
        
    class belongsToWorkshop(ObjectProperty): 
        
        range           = [Workshop]
        domain            = [Team] 
        inverse_property = consistsOfTeam
    
    class hasTechnology(ObjectProperty): 
    	range           = [Technology]
    	domain            = [SupplyChain] 
    
    class hasTimeConstraint(ObjectProperty): 
    	range           = [TimeConstraint]
    	domain            = [LotEvent] 
    
    class hasTimeConstraint(ObjectProperty): 
    	range           = [TimeConstraint]
    	domain            = [RouteSequence] 
    
    class isRestricteByKanban(ObjectProperty): 
    	range           = [Kanban]
    	domain            = [RouteSequence] 
    
    class hasWorkerQualification(ObjectProperty): 
    	range           = [WorkerQualification]
    	domain            = [Equipment] 
    Equipment.is_a.append(hasWorkerQualification.max(1,WorkerQualification))
    
    class isQualifiedByWorkerQualification(ObjectProperty): 
    	range           = [WorkerQualification]
    	domain            = [Maintenance] 
    Maintenance.is_a.append(isQualifiedByWorkerQualification.max(1,WorkerQualification))
    
    class hasWorkerQualification(ObjectProperty): 
    	range           = [WorkerQualification]
    	domain            = [Team] 
    
    class containsWorkshop(ObjectProperty): 
        range           = [Workshop]
        domain          = [Fab] 
    Fab.is_a.append(containsWorkshop.min(1,Workshop))
    
    class belongsToFab(ObjectProperty): 
        range           = [Fab]
        domain          = [Workshop]    
        inverse_property = containsWorkshop
    
    class belongsToWorkshop(ObjectProperty): 
        range           = [Workshop]
        domain          = [Equipment] 
    
    class containsEquipmentWorkshop(ObjectProperty):
        inverse_property = belongsToWorkshop
        range           = [Equipment]
        domain          = [Workshop] 
        
    class canBeProcessedIn(ObjectProperty):
        range           = [LotState]
        domain            = [CurrentWip]
    
    ###########data properties - attributes definition ####################
    
    class hasActivityType(Activity >> str): pass          # Creating some properties
    class hasAtivityID(Activity >> str, FunctionalProperty): pass
    class hasWorkplanID(Activity >> str): pass
    
    class hasProbability(ActivityEdge >> int): pass
    class hasQuantity(ActivityEdge >> int): pass
    class hasTransitionTime(ActivityEdge >> int): pass
    class hasActivityEdgeID(ActivityEdge >> str, FunctionalProperty): pass
    class hasActivityID(ActivityEdge >> str): pass
    class hasWorkplanID(ActivityEdge >> str): pass
    
    class hasQuantity(AuxResource >> int): pass
    class hasAuxResourceType(AuxResource >> str): pass
    class hasAuxResourceID(AuxResource >> str, FunctionalProperty): pass
    
    class hasQuantity(AuxResourceRequirement >> int): pass
    class hasOperationID(AuxResourceRequirement >> str): pass
    class hasAuxResourceID(AuxResourceRequirement >> str, FunctionalProperty): pass
    
    class hasBomFactor(BillOfMaterials >> int):
        comment = "Number of secondary product units assembled to one primary production unit"
        pass
    class hasBillOfMaterialsID(BillOfMaterials >> str, FunctionalProperty): pass
    class hasProductLevel4ID(BillOfMaterials >> str): pass
    
    class hasCapacityLoss(Calendar >> float): 
        comment = "Facility capacity loss in percent"
        pass
    class hasDuration(Calendar >> datetime.datetime):
        comment = "event duration in hours"
        pass
    class hasEventId(Calendar >> str): pass
    class hasEventStart(Calendar >> datetime.datetime): pass
    class hasFrequency(Calendar >> str): 
        comment = "unique, annual, quarterly, monthly, daily,..."
        pass
    class hasWorkshopID(Calendar >> str): pass
    class hasTeamID(Calendar >> str): pass
    class hasFabID(Calendar >> str): pass
    
    class hasPotentialEquipmentId(Capability >> str): pass
    class hasCapabilityID(Capability >> str, FunctionalProperty): pass
    
    class hasCapability(CapabilitySnapshot >> str): pass
    class hasCapabilityID(CapabilitySnapshot >> str): pass
    class hasEquipmentID(CapabilitySnapshot >> str): pass
    
    class hasCurrentMState(CurrentMachineState >> str): 
        comment = "Name of the actual state"
        pass
    class hasExpectedChangeDtMean(CurrentMachineState >> datetime.datetime): 
        comment = "Expected mean date for change to next state"
        pass
    class hasExpectedChangeDtVariance(CurrentMachineState >> datetime.datetime): 
        comment = "Expected variance date for change to next state"
        pass
    class hasNextState(CurrentMachineState >> str):
        comment = "Potential next state"
        pass
    class hasPrevChangeDt(CurrentMachineState >> datetime.datetime): 
        comment = "Change date from previous to current state "
        pass
    class hasEquipmentID(CurrentMachineState >> str): pass
    
    class hasCurrentMainQuantity(CurrentWip >> int): pass
    class hasCurrentState(CurrentWip >> str): pass
    class hasDemandSnapshotID(CurrentWip >> str, FunctionalProperty): pass
    class hasLotType(CurrentWip >> str): pass
    class hasPhysicalCarrierInformation(CurrentWip >> str): pass
    class hasPhysicalCarrierLocation(CurrentWip >> str): pass
    class hasPhysicalLotState(CurrentWip >> str): pass
    class hasFIFO(CurrentWip >> int): pass
    class hasEDD(CurrentWip >> datetime.time): pass
    class hasSPT(CurrentWip >> datetime.time): pass
    class hasProductLevel2IdDueDate(CurrentWip >> int): pass
    class hasRecipeChamberDedication(ChamberDedication >> str): pass
    class hasRecipeChamberDedicationAlternate(ChamberDedication >> str): pass
    class hasResidualTimeInState(CurrentWip >> int): pass
    class hasSteps(CurrentWip >> int): pass
    class hasTask(CurrentWip >> str): pass
    class hasTimeInState(CurrentWip >> datetime.time): pass
    class hasOperationID(CurrentWip >> str): pass
    class hasEquipmentID(CurrentWip >> str): pass
    class hasProductLevel4ID(CurrentWip >> str): pass
    class hasLotID(CurrentWip >> str, FunctionalProperty): pass
    class hasEquipmentStateID(CurrentWip >> str): pass
    
    class hasCycleTimeCommittment(CycleTimeCommitment >> int): pass
    class hasPeriodBegin(CycleTimeCommitment >> int): pass
    class hasPeriodEnd(CycleTimeCommitment >> int): pass
    class hasFabID(CycleTimeCommitment >> str): pass
    class hasSupplyChainID(CycleTimeCommitment >> str): pass
    class hasProductLevel4ID(CycleTimeCommitment >> str): pass
    
    class hasPeriodBegin(DeliveryCommittment >> int): pass
    class hasPeriodEnd(DeliveryCommittment >> int): pass
    class hasUnitsPerTimePeriod(DeliveryCommittment >> int): pass
    class hasFabID(DeliveryCommittment >> str): pass
    class hasSupplyChainID(DeliveryCommittment >> str): pass
    class hasProductLevel4ID(DeliveryCommittment >> str): pass
    
    class hasCurrentQuantity(DemandSnapshot >> int): pass
    class hasCustomerDueDate(DemandSnapshot >> datetime.datetime): pass
    class hasEntryDate(DemandSnapshot >> datetime.datetime): pass
    class hasOperationalDueDate(DemandSnapshot >> datetime.datetime): pass
    class hasOriginalQuantity(DemandSnapshot >> int): pass
    class hasProductLevel2DueDate(DemandSnapshot >> datetime.datetime): pass
    class hasProductLevel2Quantity(DemandSnapshot >> int): pass
    class hasProductLevel3DueDate(DemandSnapshot >> datetime.datetime): pass
    class hasProductLevel3Quantity(DemandSnapshot >> int): pass
    class hasProductLevel4DueDate(DemandSnapshot >> datetime.datetime): pass
    class hasProductLevel4Quantity(DemandSnapshot >> int): pass
    class hasTypeOfDemand(DemandSnapshot >> str): pass
    class hasDemandSnapshotID(DemandSnapshot >> str, FunctionalProperty): pass
    class hasProductLevel2ID(DemandSnapshot >> str): pass
    class hasProductLevel3ID(DemandSnapshot >> str): pass
    class hasProductLevel4ID(DemandSnapshot >> str): pass
    
    class hasCurrentQuantity(DemandTrace >> int): pass
    class hasCustomerDueDate(DemandTrace >> datetime.datetime): pass
    class hasEntryDate(DemandTrace >> datetime.datetime): pass
    class hasOriginalQuantity(DemandTrace >> int): pass
    class hasProductLevel2DueDate(DemandTrace >> datetime.datetime): pass
    class hasProductLevel2Quantity(DemandTrace >> int): pass
    class hasProductLevel3DueDate(DemandTrace >> datetime.datetime): pass
    class hasProductLevel3Quantity(DemandTrace >> int): pass
    class hasProductLevel4DueDate(DemandTrace >> datetime.datetime): pass
    class hasProductLevel4Quantity(DemandTrace >> int): pass
    class hasTimestamp(DemandTrace >> int): pass
    class hasTypeOfDemand(DemandTrace >> str): pass
    class hasProductLevel2ID(DemandTrace >> str): pass
    class hasProductLevel3ID(DemandTrace >> str): pass
    class hasProductLevel4ID(DemandTrace >> str): pass
    
    class hasTransportMode(Equipment >> str):
        comment = "Refers to the transportation mean that is used to deliver products on the considered machine (i.e. AMHS, Operator, AGV, etc."
        pass
    
    class hasDispatchRule(Equipment >> str):
        comment = "-Maybe not valid ? Refers to the system used to allocate products to the machine (can be Full-Auto, Auto-planning, etc.)"
        pass
    class hasEquipmentGroupID(Equipment >> str): 
        comment = "Name of the equipment group"
        pass
    class hasEquipmentType(Equipment >> str): pass
    class hasInternalBufferSize(Equipment >> str): 
        comment = "Used for furnaces"
        pass
    class hasLocationXY(Equipment >> str): 
        comment = "Ideally (X,Y) coordinates but can be the name of a building, floor or area"
        pass
    class hasMaxAmountOfClusteredSteps(Equipment >> int): 
        comment = "If more than one step in a sequence is operated by same equipment, it is called cluster equipment with a number of clustered steps >1; else 1"
        pass
    class hasMaxBatchSize(Equipment >> int):
        comment = "maximum number of lots for a batch tool"
        pass
    class hasMaxParallelization(Equipment >> int): 
        comment = "Number of parallel used process chambers or max parallel used load ports"
        pass
    class hasMtbf(Equipment >> int): 
        label = "hasMeanTimeBetweenFailure"
        pass
    class hasMttr(Equipment >> int): 
        label = "hasMeanTimeToRepair"
        pass
    class hasNumberOfLoadports(Equipment >> int): pass
    class hasSop(Equipment >> datetime.datetime):
        label = "hasStartOfProduction"
        pass
    class hasTrackingEquipment(Equipment >> str): pass
    class hasEquipmentID(Equipment >> str, FunctionalProperty):
        comment = "Name of the Machine"
        pass
    class hasMaintenanceID(Equipment >> str): pass
    class hasWorkshopID(Equipment >> str): pass
    class hasStage(Equipment >> str): pass
    
    class hasDisqualificationDt(EquipmentCapability >> datetime.datetime): 
        comment = "Date operation is disqualified on equipment"
        pass
    class hasQualificationDt(EquipmentCapability >> datetime.datetime):
         comment = "Date operation is qualified on equipment"
         pass
    class hasResidualQualificationPeriod(EquipmentCapability >> datetime.datetime):
        comment = "Time left until disqualification of operation "
        pass
    class hasOperationID(EquipmentCapability >> str): pass
    class hasEquipmentID(EquipmentCapability >> str): pass
    
    class hasEquipmentGroupType(EquipmentGroup >> str):
        comment = "Refers to the technology used for the process tool: Lithography, Plasma,Etching, Diffusion, Tester, Prober,... "
        pass
    class hasEquipmentGroupID(EquipmentGroup >> str, FunctionalProperty):
        comment = "Name of the workshop"
        pass
    class hasProductLevel3ID(EquipmentGroup >> str): pass
    class hasEquipmentID(EquipmentGroup >> str):
        comment = "Name of the machine"
        pass
    class hasFabID(EquipmentGroup >> str): pass
    
    class hasChangeDt(EquipmentState >> datetime.datetime): 
        comment = "Date of state change from last state to state"
        pass
    class hasLastState(EquipmentState >> str):
        comment = "Name of the equipment state (productive, standby, scheduled down)"
        pass
    class hasState(EquipmentState >> str): 
        comment = "Name of the equipment state (productive, standby, scheduled down)"
        pass
    class hasEquipmentStateID(EquipmentState >> str, FunctionalProperty): pass
    class hasEquipmentID(EquipmentState >> str): pass
    class hasMaintenanceID(EquipmentState >> str): pass
    
    class isInternal(Fab >> bool): 
        comment = "Does the Fab belong to the company or to an external partner"
        pass
    class hasMaxLots(Fab >> int): 
        comment = ""
        pass
    class hasMaxWip(Fab >> int): 
        comment = "capacity restriction of process units - 0 if no restriction"
        pass
    class hasProcessUnit(Fab >> str): 
        comment = "smalles process unit in the fab - wafer/chip/device"
        pass
    class hasFabType(Fab >> str): pass
    class hasFabID(Fab >> str, FunctionalProperty): pass
    class hasPlantID(Fab >> str): pass
    
    class hasLocationID(Location >> str, FunctionalProperty): pass
    class hasFabID(Location >> str): pass
    
    class hasEnterMainQuantity(LotEvent >> int): pass
    class hasAnEquipment(LotEvent >> str): pass
    class hasEventDate(LotEvent >> datetime.datetime): pass
    class hasEventId(LotEvent >> str): pass
    class hasEventReason(LotEvent >> int): pass
    class hasEventType(LotEvent >> str): pass
    class hasLeaveMainQuantity(LotEvent >> int): pass
    class hasParentLotId(LotEvent >> str): pass
    class hasSequenceNumber(LotEvent >> int): pass
    class hasLotID(LotEvent >> str, FunctionalProperty): pass
    class hasOperationID(LotEvent >> str): pass
    
    class hasCustomerDueDate(LotState >> datetime.datetime): pass
    class hasEnterQuality(LotState >> int): pass
    class hasEnterStateTime(LotState >> datetime.datetime): pass
    class hasEnterSubQuantity(LotState >> int): pass
    class hasAnEquipment(LotState >> str): pass
    class hasLeaveStateTime(LotState >> datetime.datetime): pass
    class hasLeaveSubQuantity(LotState >> int): pass
    class hasLotType(LotState >> str): pass
    class hasOperationalDueDate(LotState >> datetime.datetime): pass
    class hasOwner(LotState >> str): pass
    class hasParentLotId(LotState >> str): pass
    class hasPhysicalLotState(LotState >> int): pass
    class hasPriority(LotState >> int): pass
    class hasSequenceNumber(LotState >> int):
        comment = "Sequential number of the operation lot has entered"
        pass
    class hasState(LotState >> str): pass
    class hasLotStateType(LotState >> str): pass
    class hasOperationID(LotState >> str): pass
    class hasRouteSequenceID(LotState >> str): pass
    class hasProductLevel4ID(LotState >> str): pass
    class hasLotID(LotState >> str, FunctionalProperty): pass
    
    class hasDuration(Maintenance >> int): 
        comment = "Duration of the maintenance in minutes"
        pass
    class hasMaintenanceCounter(Maintenance >> int):
        comment = "counter-based => number of wafer completed / time-based => number of machine hours"
        pass
    class hasMaintenanceScheduling(Maintenance >> str):
        comment = "counter-based or time-based"
        pass
    class hasTechnicanRequirement(Maintenance >> str):
        comment = "number of technical workers required for the maintenance"
        pass
    class hasMaintenanceID(Maintenance >> str, FunctionalProperty):
        comment = "Name of the maintenance"
        pass
    class hasEquipmentID(Maintenance >> str):
        comment = "Name of the equipment that needs maintenance"
        pass
    
    class hasMaintenanceVolume(Maintenance >> str):
        comment = "volume-based Maintenance"
        pass
    
    class hasDeliveryTime(Material >> int): pass
    class hasMaxStock(Material >> int): pass
    class hasMinStock(Material >> int): pass
    class hasQuantity(Material >> int): pass
    class hasRecoveryTime(Material >> int): pass
    class hasMaterialType(Material >> str): pass
    class hasUnit(Material >> str): pass
    class hasMaterialID(Material >> str, FunctionalProperty): pass
    
    class hasMinQuantity(MaterialConsumption >> int): pass
    class hasQuantity(MaterialConsumption >> int): pass
    class hasUnit(MaterialConsumption >> int): pass
    class hasOperationID(MaterialConsumption >> str): pass
    class hasMaterialID(MaterialConsumption >> str): pass
    
    class hasMaterialTransferTime(MaterialTransfer >> datetime.datetime): 
        comment = "Duration of transfer from one to the other equipment"
        pass
    class hasTransferMode(MaterialTransfer >> str): 
        comment = "Automation, AGV, milkrun or manual"
        pass
    
    class hasTransportFromLocation(MaterialTransfer >> str):
        pass
    class hasTransportToLocation(MaterialTransfer >> str):
        pass
    
    class hasMaximumLotSize(MaxLotSize >> int): pass
    class hasMinimumLotSize(MaxLotSize >> int): pass
    class hasFabID(MaxLotSize >> str): pass
    class hasProductLevel3ID(MaxLotSize >> str): pass
    
    class hasCycleTimePerUnit(Operation >> datetime.datetime): 
        comment = "Overall cycle time spent on the tool (incl handling etc.)"
        pass
    class hasNextClusterOperation(Operation >> str):
        comment = "If operation is part of a cluster sequence, name of next cluster operation"
        pass
    class hasProcessingTimePerUnit(Operation >> datetime.datetime):
        comment = "Capacity consuming / relevant equipment processing time"
        pass
    class hasSamplingRate(Operation >> int):
        comment = "Execution rate of operation If operation is mandatory sampling rate is 1 (1-sampling rate = skipping rate of optional operation)"
        pass
    class hasVarianceCyclingTime(Operation >> datetime.datetime):
        comment = "Variance of overall operation cycle time"
        pass
    class hasVarianceProcessingTime(Operation >> datetime.datetime):
        comment = "Variance of capacity relevant operation processing time"
        pass
    class hasYieldProbability(Operation >> int):
        comment = "Yield rate of the operation in percent"
        pass
    class hasOperationID(Operation >> str, FunctionalProperty):
        comment = "Name of the operation / process step"
        pass
    class hasCapabilityID(Operation >> str): pass
    class hasRecipeID(Operation >> str): pass
    
    class isInternal(Plant >> bool): 
        comment = "Does the plant belong to the company or to an external partner"
        pass
    class hasPlantID(Plant >> str, FunctionalProperty): 
        comment = "Geographical location of the plant"
        pass
    class hasSupplyChainID(Plant >> str): pass
    
    class hasProductLevel2ID(ProductLevel2 >> str, FunctionalProperty):
        comment = "Name of the product in level 2"
        pass
    class hasTechnologyID(ProductLevel2 >> str): 
        pass
    
    class hasMaxUnitsPerWeek(ProductLevel3 >> int):
        comment = "Maximum of unit completes (Wafers / chips) per week on the bottleneck equipment group and thereby on the product"
        pass
    class hasProductLevel3ID(ProductLevel3 >> str, FunctionalProperty):
        comment = "Name of the product in level 3"
        pass
    class hasProductLevel2ID(ProductLevel3 >> str): pass
    
    class hasChipsPerWafer(ProductLevel4 >> int):
        comment = "number of chips on a wafer"
        pass
    class hasProductLevel4ID(ProductLevel4 >> str, FunctionalProperty): 
        comment = "Name of the product in the highest granularity"
        pass
    class hasWorkplanID(ProductLevel4 >> str): pass
    class hasProductLevel3ID(ProductLevel4 >> str): pass
    class hasReleasePlanID(ProductLevel4 >> str): pass
    
    class hasRecipeID(Recipe >> str, FunctionalProperty): pass
    class hasRecipeGroupID(Recipe >> str): pass
    
    class hasCTime(RecipeEquipmentConfig >> int): pass
    class hasMaxBatchSize(RecipeEquipmentConfig >> int): pass
    class hasMinBatchSize(RecipeEquipmentConfig >> int): pass
    class hasPTime(RecipeEquipmentConfig >> int): pass
    class hasRecipeMax(RecipeEquipmentConfig >> int): pass
    class hasRecipeID(RecipeEquipmentConfig >> str): pass
    class hasEquipmentID(RecipeEquipmentConfig >> str): pass
    
    class hasRecipeGroupID(RecipeGroup >> str, FunctionalProperty): pass
    
    class hasPeriodBegin(ReleasePlan >> int): pass
    class hasPeriodEnd(ReleasePlan >> int): pass
    class hasUnitsPerTimePeriod(ReleasePlan >> int): pass
    class hasReleasePlanID(ReleasePlan >> str, FunctionalProperty): pass
    class hasFabID(ReleasePlan >> str): pass
    class hasSupplyChainID(ReleasePlan >> str): pass
    
    class hasRouteID(Route >> str, FunctionalProperty):
        comment = "Name of route (usually equals product names requiring the route)"
        pass
    class hasRouteSequenceID(Route >> int, FunctionalProperty):
        comment = "Sequential number of operations along the route"
        pass
    class hasProductLevel4ID(Route >> str): pass
    
    class hasBlock(RouteSequence >> str): pass
    class hasLinkedSequenceNumber(RouteSequence >> int): pass
    class hasSequenceNumber(RouteSequence >> int): pass
    class hasRouteSequenceID(RouteSequence >> str, FunctionalProperty): pass
    class hasOperationID(RouteSequence >> str): pass
    
    class hasDuration(Setup >> int):
        comment = "Duration of the setup"
        pass
    class hasSetupID(Setup >> str, FunctionalProperty): pass
    class hasRecipeGroupID(Setup >> str): pass
    class hasEquipmentGroupID(Setup >> str): pass
    
    class hasShiftStart(ShiftAttendance >> datetime.datetime):
        comment = "Date and time when the shifts starts"
        pass
    class hasWorkforceOperational(ShiftAttendance >> int):
        comment = "Overall number of operational workers assigned to the shift"
        pass
    class hasWorkforceTechnical(ShiftAttendance >> int): 
        comment = "Overall number of technical workers assigned to the shift"
        pass
    
    class hasTransitTime(SupplyChain >> int):
        comment = ""
        pass
    class hasSupplyChainID(SupplyChain >> str, FunctionalProperty):
        comment = "Name of the supply chain."
        pass
    class hasFabID(SupplyChain >> str): 
        comment = "Name of the production unit."
        pass
    
    class hasFinishedProductCostsPerUnit(TargetCosts >> int): pass
    class hasPeriodBegin(TargetCosts >> int): pass
    class hasPeriodEnd(TargetCosts >> int): pass
    class hasWipCostsPerUnitAndTimeUnit(TargetCosts >> int): pass
    class hasTechnologyID(TargetCosts >> str): pass
    
    class hasPeriodBegin(TargetRevenue >> int): pass
    class hasPeriodEnd(TargetRevenue >> int): pass
    class hasRevenuePerUnit(TargetRevenue >> int): pass
    class hasTechnologyID(TargetRevenue >> str): pass
    
    class hasBetaServiceLevel(TargetServiceLevel >> int): pass
    class hasFillRate(TargetServiceLevel >> int): pass
    class hasPeriodBegin(TargetServiceLevel >> int): pass
    class hasPeriodEnd(TargetServiceLevel >> int): pass
    class hasStockoutProbability(TargetServiceLevel >> int): pass
    class hasTechnologyID(TargetServiceLevel >> str): pass
    
    class hasTargetMaxAmountOfUnits(TargetWip >> int): pass
    class hasTargetMinAmountOfUnits(TargetWip >> int): pass
    class hasProductLevel4ID(TargetWip >> str): pass
    
    class hasDurationOff(Team >> int): pass
    class hasWorkforceOperational(Team >> int): pass
    class hasWorkforceTechnical(Team >> int): pass
    class hasTeamID(Team >> str, FunctionalProperty): pass
    class hasWorkshopID(Team >> str): pass
    
    class hasTechnologyID(Technology >> str, FunctionalProperty):
        comment = "Name of the technology"
        pass
    class hasSupplyChainID(Technology >> str):
        comment = "Assignment of technology to supply chain"
        pass
    
    class hasEndSequenceNumber(TimeConstraint >> int):
        comment = "sequence number in the route time window ends"
        pass
    class hasMaximumTime(TimeConstraint >> datetime.datetime):
        comment = "duration"
        pass
    class hasMinimumTime(TimeConstraint >> datetime.datetime): pass
    class hasStartSequenceNumber(TimeConstraint >> int): 
        pass
    class hasRouteSequenceID(TimeConstraint >> str): pass
    class hasLotID(TimeConstraint >> str): pass

    class hasMaxLotAmount(Kanban >> str): pass
    class hasKanbanID(Kanban >> str): pass
    class hasMaxWaferAmount(Kanban >> str): pass
    
    class hasWorkforce(WorkerQualification >> int): 
        commen = "Number of workers necessary to maintain / run equipment"
        pass
    class hasEquipmentID(WorkerQualification >> str): pass
    class hasMaintenanceID(WorkerQualification >> str): pass
    class hasTeamID(WorkerQualification >> str): pass
    
    class hasWorkplanID(Workplan >> str, FunctionalProperty): pass
    
    class isInternal(Workshop >> bool): 
        comment = "Does the workshop belong to the company or to a production partner"
        pass
    class hasShiftModel(Workshop >> str): pass
    class hasWorkshopType(Workshop >> str): 
        comment = "Functional/Divisional"
        pass
    class hasWorkDay(Workshop >> int):
        comment = "Productive days per week"
        pass
    class hasWorkingHourPerDay(Workshop >> int):
        comment = "Productive hours per day"
        pass
    class hasWorkshopID(Workshop >> str, FunctionalProperty):
        comment = "Name of the Workshop"
        pass
    class hasFabID(Workshop >> str):
        comment = "Name of the fab assigned to the workshop"
        pass
    
    
sync_reasoner_pellet(infer_property_values = True, infer_data_property_values = True)

onto.save("gdm.owl",format = "rdfxml")


